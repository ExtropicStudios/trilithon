<?

if (PHP_INT_MAX != 9223372036854775807) {
	die("Must be run on a 64-bit platform");
}

# Uncomment this and put your private Pinboard email here
#define('PINBOARD_EMAIL', 'post+your+name+secret@pinboard.in');

# Add your phone number to this list to allow posting notes from it.
# Phone numbers should be in the format +15555551234
define('AUTHORIZED_NUMBERS', [
]);

# temporary until i get a real database up
#define('USER_API_KEY', '');

# Uncomment this and put your Twilio account SID to authorize Twilio requests
#define('TWILIO_ACCOUNT_SID', 'supersecrethash');

# Uncomment this to get some super basic Plivo request validation.
# Plivo doesn't provide an SID so this is next to useless.
#define('PLIVO_NUMBER', '+15555551234');
