<?

require __DIR__ . '/vendor/autoload.php';
require_once './config.php';

use Extropic\Models\PlainTextNoteAccessor;

if ($_GET['key'] !== USER_API_KEY) {
	http_response_code(401);
	die("Invalid API key");
}

$accessor = new PlainTextNoteAccessor();

$notes = $accessor->get_page(0, $_GET['since_id'] ?? "");

print json_encode($notes, JSON_PRETTY_PRINT);
