# Selecting an SMS provider

Current options:

### Twilio

Pros:

- Includes an account secret that can be used to verify the source of messages
- PHP library has no additional dependencies
- Web dashboard has a sweet request debugger
- Can respond to messages directly from the webhook

Cons:

- Expensive as h\*ck

### Nexmo

Pros:

- Almost as cheap as Plivo
- They have a CLI client for interacting with dev stuff. nice.
- Has full nonce+timestamp+signature authentication on webhook requests, nice!

Cons:

- PHP library pulls in Guzzle... jesus guys get your shit together.
- You can't respond to a message directly from the webhook

### Plivo

Pros:

- Cheap as h\*ck
- Can respond to messages directly from the webhook

Cons:

- PHP library pulls in Guzzle so that I can write out a frickin response? I could just write the XML directly though.
- No account secret, no way to verify source of messages. I can replicate this by putting a secret in the webhook though.
