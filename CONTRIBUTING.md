ways to contribute:

1. open a pull request (easiest)
2. email me a patch file <josh@extropicstudios.com>
3. carve your changes into a rock and strap it to a passing drone