<?php

namespace Extropic\SMS;

/**
 * Three days into this project and I've already got a god damn factory pattern
 * what is wrong with me
 */
class ProviderFactory {

	/**
	 * @param array $request The global $_REQUEST
	 * @param array $get The global $_GET
	 * @param array $post The global $_POST
	 *
	 * @return The provider who should handle this request, or null if none.
	 */
	public static function getProvider(array $request, array $get, array $post): ?Provider {
		$provider = new Twilio();
		if ($provider->identify($request, $get, $post)) {
			return $provider;
		}

		$provider = new Plivo();
		if ($provider->identify($request, $get, $post)) {
			return $provider;
		}

		return null;
	}

}
