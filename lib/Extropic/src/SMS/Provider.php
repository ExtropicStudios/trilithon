<?php

namespace Extropic\SMS;

interface Provider {

	/**
	 * Determines whether a given provider is responsible for initiating the
	 * current webhook request.
	 *
	 * @param array $request The global $_REQUEST
	 * @param array $get The global $_GET
	 * @param array $post The global $_POST
	 *
	 * @return True if this provider initiated the request.
	 */
	public function identify(array $request, array $get, array $post): bool;

	/**
	 * Generates a webhook response from a string.
	 *
	 * @param string $msg The message to send back to the user.
	 *
	 * @return string The content to return from the webhook, usually XML.
	 */
	public function generate_response(string $msg): string;

}
