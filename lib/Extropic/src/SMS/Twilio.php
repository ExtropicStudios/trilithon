<?php

namespace Extropic\SMS;

class Twilio implements Provider {

	public function identify(array $request, array $get, array $post): bool {

		// TWILIO_ACCOUNT_SID and AUTHORIZED_NUMBERS should be defined in
		// config.php
		return defined('TWILIO_ACCOUNT_SID')
			&& !empty($request['AccountSid'])
			&& TWILIO_ACCOUNT_SID == $request['AccountSid']
			&& in_array($_REQUEST['From'], AUTHORIZED_NUMBERS);
	}

	public function generate_response(string $msg): string {
		$response = new \Twilio\Twiml;
		$response->message($msg);
		return $response->__toString();
	}

}
