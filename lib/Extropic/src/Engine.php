<?

namespace Extropic;

use DateTime;
use Extropic\Models\Note;
use Extropic\Models\PlainTextNoteAccessor;

class Engine {

	const URL_REGEX = '/(https?:\/\/[^\s]+)/';

	/**
	 * Entry point to the engine
	 *
	 * @param array $request The request object from Twilio
	 * @return string The message to send back to the user.
	 **/
	public static function run(array $request): string {
		$start = microtime(true);
		$body = $request['Body'];

		if (strlen($body) <= 0) {
			return 'No note body detected. Note was not saved.';
		}

		if (strlen($body) > 1000) {
			return 'Notes are limited to 1000 characters. Note was not saved.';
		}

		$base_msg = Engine::get_base_msg($start, $request);
		#$affirmed_msg = $base_msg . "\n\n" . Engine::get_affirmation();

		#if (strlen($affirmed_msg) <= 160) {
		#	return $affirmed_msg;
		#} else {
			return $base_msg;
		#}
	}

	private static function get_base_msg(int $start, array $request): string {
		$body = $request['Body'];
		if (strpos($body, 'email ') === 0 && $msg = Engine::action_email($request['From'], substr($body, 6))) {
			return $msg;
		}

		return Engine::action_note($start, $body);
	}

	private static function get_affirmation(): string {

		$affirmation_intro = [
			'And remember',
			'Don\'t forget',
		];

		$affirmation_joins = [
			': ',
			'! ',
		];

		$affirmations = [
			'The most important step is showing up.',
			'Trust the process.',
			'Every artwork is just a history of mistakes.',
			'You can have anything you want, if you\'re willing to pay the price.',
			'You are capable of more than you know.',
		];

		$intro = $affirmation_intro[mt_rand(0, count($affirmation_intro) - 1)];
		$join = $affirmation_joins[mt_rand(0, count($affirmation_joins) - 1)];
		$aff = $affirmations[mt_rand(0, count($affirmations) - 1)];
		return $intro . $join . $aff;
	}

	/**
	 * Send an email on behalf of the user.
	 *
	 * @param string $from The identifier of the user
	 * @param string $body The send command to parse.
	 * @return null|string
	 */
	private static function action_email(string $from, string $body): ?string {
		$email = explode(' ', $body)[0];

		# well, it's better than nothing.
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return null;
		}

		$msg = substr($body, strlen($email) + 1);
		if (mail($email, 'A message from ' . $from, $msg, 'From: ' . $from . ' <no-reply@extropicstudios.com>')) {
			error_log('Sent an email to ' . $email . ' on behalf of ' . $from);
			return 'Email sent to ' . $email . '!';
		} else {
			return 'Email failed to send. Format: \'email <user@website.com> message content\'';
		}
	}

	/**
	 * Save a note to the database.
	 */
	private static function action_note(float $start, string $body): string {

		$accessor = new PlainTextNoteAccessor();

		$id = Note::generateID();
		$note = Note::construct($id, $body, time());
		$result = $accessor->insert($note);

		if ($result === false) {
			return 'Note failed to save! Try again.';
		}

		$bookmarks_saved = 0;

		// Try and parse any URLs out of the note and mail them to Pinboard.
		if (defined('PINBOARD_EMAIL')) {
			preg_match_all(Engine::URL_REGEX, $body, $urls);
			$title_prefix = preg_replace(Engine::URL_REGEX, '', $body);
			foreach ($urls[0] as $i => $url) {
				if (count($urls[0]) > 1) {
					$title = $title_prefix . ' (' . ($i + 1) . ' of ' . count($urls[0]) . ')';
				} else {
					$title = $title_prefix;
				}
				if (mail(PINBOARD_EMAIL, $title, $url . "\nSubmitted via Extropic Notes.\nsms-notes t p")) {
					$bookmarks_saved++;
				}
			}
		}

		$notes = count(file('notes.txt', FILE_SKIP_EMPTY_LINES));
		#$bo = $result - (strlen($prefix) + strlen($suffix));
		#$end = microtime(true);
		#$msg = $bo . ' characters saved to note in ' . number_format($end - $start, 2) . 's.';
		$msg = 'Note saved!';
		if ($bookmarks_saved > 0) {
			$msg .= ' ' . $bookmarks_saved . ' bookmarks saved to Pinboard.';
		}
		$msg .= ' ' . $notes . ' notes awaiting sync.';
		return $msg;
	}
}
