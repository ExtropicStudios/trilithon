<?php

namespace Extropic\Models;

use DateTime;

class PlainTextNoteAccessor implements NoteAccessor {

	public function get_page(string $user_id, ?string $last_note_id): array {
		$notes = file('notes.txt', FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);

		$idx = 0;
		if ($last_note_id != null) {
			foreach ($notes as $i => $n) {
				if (substr($n, 0, Identifiable::ID_STRING_LENGTH) === $last_note_id) {
					$idx = $i + 1;
					break;
				}
			}
		}

		$latest_notes = array_slice($notes, $idx);

		return array_map(function($n) {
			$fields = explode("\t", $n);

			assert(count($fields) == 3);

			$id = $fields[0];
			$created_on = strtotime($fields[1]);
			$message = $fields[2];

			return Note::construct($id, $message, $created_on);
		}, $latest_notes);
	}

	public function insert(Note $note): bool {
		$message = join("\t", [
			$note->getID(),
			date(DateTime::COOKIE, $note->getCreatedOn()),
			$note->getMessage()
		]);
		return file_put_contents('notes.txt', "{$message}\n", FILE_APPEND | LOCK_EX);
	}
}
