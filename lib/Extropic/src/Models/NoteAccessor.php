<?php

namespace Extropic\Models;

interface NoteAccessor {

	/**
	 * @param string $user_id
	 * @param string|null $last_note_id
	 *
	 * @return Note[]
	 */
	public function get_page(string $user_id, ?string $last_note_id): array;

	public function insert(Note $note): bool;

}
