<?php

namespace Extropic\Models;

use Extropic\Bits;

abstract class Identifiable {

	const ID_STRING_LENGTH = 22;

	/**
	 * @var string
	 */
	private $id;

	protected function __construct(string $id) {
		$this->id = $id;
	}

	/**
	 * @param int $now UNIX timestamp.
	 * @param int $seed
	 * @return string
	 */
	public static function generateID(?int $now = null, ?int $seed = null): string {
		assert($now >= 0);

		if ($now === null) {
			$now = time();
		}

		if ($seed !== null) {
			mt_srand($seed);
		}

		$time_bits = Bits::intToBinary($now, 64);
		$random_bits = Bits::intToBinary(mt_rand(), 32) . Bits::intToBinary(mt_rand(), 32);

		$id_bits = $time_bits . $random_bits;

		$id = Bits::binaryToBase62($id_bits, Identifiable::ID_STRING_LENGTH);

		return $id;
	}


	public function getID(): string {
		return $this->id;
	}

}
