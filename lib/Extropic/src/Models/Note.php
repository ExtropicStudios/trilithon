<?php

namespace Extropic\Models;

use DateTime;

class Note extends Identifiable implements \JsonSerializable {

	/**
	 * @var string
	 */
	private $message;

	/**
	 * @var int Unix timestamp
	 */
	private $created_on;

	public static function construct(string $id, string $message, int $created_on): Note {
		$n = new Note($id);
		$n->message = $message;
		$n->created_on = $created_on;
		return $n;
	}

	public function jsonSerialize() {
		return [
			'id' => $this->getID(),
			'message' => $this->message,
			'created_on' => date(DateTime::COOKIE, $this->created_on),
		];
	}

	/**
	 * @return int
	 */
	public function getCreatedOn(): int {
		return $this->created_on;
	}

	/**
	 * @return string
	 */
	public function getMessage(): string {
		return $this->message;
	}
}
