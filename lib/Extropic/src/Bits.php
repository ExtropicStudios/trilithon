<?php

namespace Extropic;

/**
 * Functions for operating on numeric string representations.
 */
class Bits {

	/**
	 * Converts a binary string into a Base62 string, padding with zeroes until
	 * it reaches the specified width.
	 */
	public static function binaryToBase62(string $binary_string, int $width): string {
		return str_pad(gmp_strval(gmp_init($binary_string, 2), 62), $width, "0", STR_PAD_LEFT);
	}

	/**
	 * Converts an integer into a binary string, padding with zeroes until it
	 * reaches the specified width.
	 */
	public static function intToBinary(int $int, int $width): string {
		return str_pad(gmp_strval(gmp_init($int, 10), 2), $width, "0", STR_PAD_LEFT);
	}

}
