# sms.extropicstudios.com

do things with sms

## current features

1. send email - `email <someone@someplace.com> message content`
2. save notes - anything that doesn't match the above patterns
3. save bookmarks to pinboard - just send a url in a note

## how to deploy

this is designed to run on nearlyfreespeech.net, it might not work perfectly
on other web hosts.

1. make sure dependencies are installed/configured: php 7.1, composer
2. unzip / clone the files somewhere
3. run composer install
4. get a free twilio number and point the webhook at index.php
5. edit config.php and put your phone number in there
6. that's it probably (unless i forgot something)
7. now go outside, touch some fuzzy animals, and sms yourself silly

## potential future features

1. hail a cab - `ride <address>` via [lyft API](https://developer.lyft.com/docs/getting-started)
2. daily habit tracking
3. calendar management
4. TODO list
5. Multi User Dungeons (with in-app purchases of course)
6. Fully Realized Clippy Emulator / Resurrection Of The One True Digital Assistant

## never future features

1. post to social media (too tempting)
2. lethal drone strikes (potential ethical implications)
3. calculator (just use your watch, dummy!)
4. bitcoin anything (anti-hype, actually i'm just bitter cause i missed out)
