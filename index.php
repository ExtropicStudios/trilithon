<?
require __DIR__ . '/vendor/autoload.php';
require_once './config.php';

use Extropic\Authorization;
use Extropic\Engine;
use Extropic\SMS\ProviderFactory;

$provider = ProviderFactory::getProvider($_REQUEST, $_GET, $_POST);

if ($provider === null) {
	die();
}

// west coast best coast
date_default_timezone_set('America/Los_Angeles');

$msg = Engine::run($_REQUEST);

print $provider->generate_response($msg);

