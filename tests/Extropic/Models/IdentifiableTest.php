<?php

namespace Extropic\Models;

use PHPUnit\Framework\TestCase;

class IdentifiableTest extends TestCase {

	public function testIDGeneratesCorrectly() {
		$actual = Identifiable::generateID(1, 99);
		$expected = '00000000000TMjROKCfjKD';

		$this->assertEquals($expected, $actual);
	}
}
