<?php

namespace Extropic\Models;

use PHPUnit\Framework\TestCase;

class NoteTest extends TestCase {

	public function testNotesSerializeCorrectly() {
		$id = Note::generateID(1,1);
		$note = Note::construct($id,"this is a cool note", strtotime("24 Nov 2017 17:12:00"));

		$actual = json_encode($note);

		$expected = '{"id":"' . $id . '","message":"this is a cool note","created_on":"Friday, 24-Nov-2017 17:12:00 UTC"}';

		$this->assertEquals($expected, $actual);
	}
}
