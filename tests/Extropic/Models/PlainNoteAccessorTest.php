<?php

namespace Extropic\Models;

use PHPUnit\Framework\TestCase;

class PlainNoteAccessorTest extends TestCase {

	public function testNoteAccessorCanGetPage() {

		// TODO: refactor so test doesn't need knowledge of underlying persistence
		if (file_exists('notes.txt')) {
			unlink('notes.txt');
		}

		$accessor = new PlainTextNoteAccessor();

		$id = Note::generateID(1,1);
		$note = Note::construct($id, "test", 1234);
		$accessor->insert($note);

		$actual = $accessor->get_page(0, 0);

		// TODO: refactor to compare two Note objects instead of json
		$expected = '[{"id":"' . $id . '","message":"test","created_on":"Thursday, 01-Jan-1970 00:20:34 UTC"}]';

		$this->assertEquals($expected, json_encode($actual));
	}
}
